Опишіть, як можна створити новий HTML тег на сторінці.
--------------------
Створити новий тег за допомогою методу createElement():
let newTag = document.createElement('my-tag');

Встановити необхідні атрибути для тега, використовуючи метод setAttribute(), якщо потрібно:

newTag.setAttribute('class', 'my-class');
newTag.setAttribute('data-attribute', 'my-data');

Додати вміст для тега за допомогою методу innerHTML, якщо потрібно:
newTag.innerHTML = ' тег';
Додати новий тег до DOM дерева за допомогою методу appendChild():
document.body.appendChild(newTag);


Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
-------------------------------
insertAdjacentHTML - це метод елемента DOM, який дозволяє вставляти HTML-рядки в певне місце вмісту елемента. Перший параметр методу insertAdjacentHTML() визначає, де буде вставлено HTML-рядок. Цей параметр має чотири можливих значення:

"beforebegin" - вставляє HTML-рядок перед початком елементу, який викликає цей метод.
"afterbegin" - вставляє HTML-рядок в початок вмісту елементу, який викликає цей метод.
"beforeend" - вставляє HTML-рядок в кінець вмісту елементу, який викликає цей метод.
"afterend" - вставляє HTML-рядок після елементу, який викликає цей метод.

Як можна видалити елемент зі сторінки?
-------------------------------

Для видалення елемента зі сторінки можна використати метод remove() або метод parentNode.removeChild().

let element = document.getElementById('my-element');
element.remove();


Метод parentNode.removeChild():
let element = document.getElementById('my-element');
element.parentNode.removeChild(element);
