function displayList(arr, parent = document.body) {
  const list = document.createElement('ul');
  parent.appendChild(list);
  arr.forEach(item => {
    const listItem = document.createElement('li');
    if (Array.isArray(item)) {
      displayList(item, listItem);
    } else {
      listItem.textContent = item;
    }
    list.appendChild(listItem);
  });
}

const arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arr2 = ["1", "2", "3", "sea", "user", 23];
const arr3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

displayList(arr1);
displayList(arr2);
displayList(arr3);
let secondsLeft = 3;
const countdown = setInterval(() => {
  console.log(secondsLeft);
  secondsLeft--;
  if (secondsLeft === 0) {
    clearInterval(countdown);
    document.body.innerHTML = '';
  }
}, 1000);